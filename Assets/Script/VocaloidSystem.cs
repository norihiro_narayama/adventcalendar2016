﻿using UnityEngine;
using System;

namespace AdventCalendar2016 {

	public class VocaloidSystem {
		/**
	 	 * @brief	Get resource path.
		 */
		public static string GetVOCALOIDResourcePath() {
			string path = Application.streamingAssetsPath + "/VOCALOID";
			return path;
		}

		/**
		 * @brief	Get Vsqx path.
		 */
		public static string GetDBIniPath() {
			string path = GetVOCALOIDResourcePath() + "/DB_ini";
			return path;
		}

		/**
		 * @brief	Get SequenceFiles path.
		 */
		public static string GetSequenceFilesPath() {
			string path = GetVOCALOIDResourcePath() + "/SequenceFiles";
			return path;
		}

		/**
	 	 * @brief	ティックを時間[sec]に変換
	 	 * @param	tick ティック
	 	 * @param	bpm BPM
	 	 * @return 	時間[sec]
	 	 */
		public static Single TickToTime( Int32 tick, Single bpm ) {
			return ( Single )( tick / ( 8.0 * bpm ) );
		}
			
		/**
	 	 * @brief	小節数をtickに変換
	 	 * @param	measure	小節数
	 	 * @return 	ティック
	 	 */
		public static Int32 MeasureToTick( Int32 measure ) {
			// デフォルトは4分の4拍子.
			return 480 * 4 * measure;
		}
	}
}
