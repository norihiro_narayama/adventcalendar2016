# README #
使い方
---
真ん中のボタンを押すとユニティちゃんの歌声が聞けます

*unityのバージョンは5.5です

*VOCALOID SDKのバージョンは1.3.0です

動作には別途VOCALOID SDKをダウンロードし、インポートしてください。

http://business.vocaloid.com/unitysdk/download/

---
本コンテンツにはヤマハ株式会社の
「VOCALOID SDK for Unity」が使用されています。 

「VOCALOID(ボーカロイド)」ならびに「ボカロ」はヤマハ株式会社の登録商標です。