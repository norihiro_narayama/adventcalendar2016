﻿using System;
using System.Collections.Generic;

#if UNITY_EDITOR_WIN || UNITY_STANDALONE_WIN
using Yamaha.VOCALOID.Windows;
#elif UNITY_EDITOR_OSX || UNITY_STANDALONE_OSX
using Yamaha.VOCALOID.OSX;
#elif UNITY_IOS
using Yamaha.VOCALOID.iOS;
#endif

namespace AdventCalendar2016 {
	public class VocaloidAudioManager {

		public enum EnginState {
			Uninitialized, Initialized, Finalized, EnginStateLength
		};
		private EnginState eState = EnginState.Uninitialized;	// エンジンの状態.
		public EnginState EState {
			get {
				return this.eState;
			}
		}

		private VocaloidAudio vAudio = null;
		public VocaloidAudio VAudio {
			get {
				return vAudio;
			}
		}

		private List<YVF.YVFSinger> singerList = new List<YVF.YVFSinger>();
			
		/**
		 * @brief	エンジンを起動
		 */
		public bool Startup( Int32 engineCount ) {
			if (eState == EnginState.Uninitialized || eState == EnginState.Finalized){
				YVF.YVFResult result = YVF.YVFStartup("personal", VocaloidSystem.GetDBIniPath(), singerList);
				if ( result != YVF.YVFResult.Success ) {
					return false;
				}
				eState = EnginState.Initialized;

				// Playbackモードに設定する.
				YVF.YVFSetStaticSetting( engineCount );
				return true;
			}
			return false;
		}

		/**
		 * @brief	エンジンを停止
		 */
		public void Shutdown() {
			if ( EState == EnginState.Initialized ) {
				YVF.YVFShutdown();
				eState = EnginState.Finalized;
			}
		}

		/**
		 * @brief	歌声合成の実行
		 */
		public void Render(VocaloidAudio vocaloidAudio) {
			if( vAudio != null ) {
				vAudio.Delete();
			}
			vAudio = vocaloidAudio;
			vAudio.SetupRender( 0, singerList[0] );
		}

		/**
		 * @brief	合成音，バッキングの再生
		 */
		public void play() {
			if ( vAudio != null ) {
				vAudio.Sing();
			}
		}

	}

}
