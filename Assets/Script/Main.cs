﻿using UnityEngine;
using UnityEngine.UI;

namespace AdventCalendar2016 {
	public class Main : MonoBehaviour {

		[SerializeField] Button SingButton;
		[SerializeField] VocaloidAudio vocaloidAudio;
		VocaloidAudioManager audioManager = null;
		private bool ready = false;
	
		// Use this for initialization
		void Start () {
			// オーディオマネージャの初期化.
			audioManager = new VocaloidAudioManager();
			ready = audioManager.Startup(1); // 起動する合成エンジンの数.
		}
		
		void FixedUpdate() {
			if ( audioManager == null ) { return; }

			if( audioManager.VAudio != null && audioManager.VAudio.RState == VocaloidAudio.RenderState.Ready ) {
				audioManager.play();
				SingButton.interactable = true;
			}
		}

		void OnApplicationQuit() {
			if ( audioManager == null ){ return; }

			audioManager.Shutdown();
		}

		/**
		 * @brief	歌声合成を行い，再生する.
		 */
		public void PlayMusic() {
			if ( ready ) {	
				if ( audioManager.EState == VocaloidAudioManager.EnginState.Initialized ) {

					SingButton.interactable = false;
					audioManager.Render( vocaloidAudio );

				}
			}
		}
	}
}