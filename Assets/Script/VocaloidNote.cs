﻿using System;

#if UNITY_EDITOR_WIN || UNITY_STANDALONE_WIN
using Yamaha.VOCALOID.Windows;
#elif UNITY_EDITOR_OSX || UNITY_STANDALONE_OSX 
using Yamaha.VOCALOID.OSX;
#elif UNITY_IOS
using Yamaha.VOCALOID.iOS;
#endif

namespace AdventCalendar2016 {

	public class VocaloidNote {

		public YVF.YVFNote Note = new YVF.YVFNote();

		public YVF.YVFVibData VibData = new YVF.YVFVibData(); 

		public VocaloidNote() {
			ClearNote();
		}

		public VocaloidNote( VocaloidNote source ) {
			Clone( source );
		}
			
		public void Clone( VocaloidNote source ) {
			this.Note = source.Note;
		}

		/**
		 * @brief	終了時刻を返す
		 * @return	終了時刻
		 */
		public Int32 GetEndTime() {
			return Note.noteTime + Note.noteLength;
		}
	
		/**
		 * @brief	ノート情報をクリアする
		 */
		public void ClearNote() {
			Note.noteTime = 0;
			Note.noteLength = 0;
			Note.noteNumber = 0;
			Note.noteVelocity = 0;
			// vibrato
			Note.vibTypeno = 0;
			Note.vibDelay = 0;
			// lyric
			Note.lyricStr = "";
			Note.phoneticStr = "";
			Note.protect = 0;
			// expression
			Note.bendDepth = 0;
			Note.bendLength = 0;
			Note.upPortamento = 0;
			Note.downPortamento = 0;
			Note.decay = 0;
			Note.accent = 0;
			Note.opening = 0;
		}
	}
}
