﻿using System;

namespace AdventCalendar2016 {

	public class VocaloidException:Exception {

		private Int32 handle = 0;
		public Int32 Handle {
			get {
				return this.handle;
			}
		}

		public VocaloidException( string message, Int32 handle ): base( message ) {
			this.handle = handle;
		}
	}
}
