﻿/**
 * Post Process Build
 *
 * Copyright(C) 2015 Yamaha Corporation. All rights reserved.
 *
 * @file   PostProcessBuild.cs
 */

#if UNITY_IOS
using UnityEngine;
using UnityEditor;
using UnityEditor.Callbacks;
using System.Collections;
using System.IO;
using UnityEditor.iOS.Xcode;

public class PostProcessBuild : MonoBehaviour
{
	/**
	 * @brief [PostProcessBuild]
	 */
	[PostProcessBuild]
	public static void OnPostprocessBuild(BuildTarget buildTarget, string path)
	{
		if (buildTarget == BuildTarget.iOS)
		{
			string projPath = path + "/Unity-iPhone.xcodeproj/project.pbxproj";

			PBXProject proj = new PBXProject();
			proj.ReadFromString(File.ReadAllText(projPath));

			string target = proj.TargetGuidByName("Unity-iPhone");

			string fileGuid = proj.AddFile("../Assets/VOCALOID_SDK/iOS/Libraries/libxml2.2.dylib", "Libraries/libxml2.2.dylib", PBXSourceTree.Source);
			proj.AddFileToBuild(target, fileGuid);

			fileGuid = proj.AddFile("../Assets/VOCALOID_SDK/iOS/Libraries/libc++.1.dylib", "Libraries/libc++.1.dylib", PBXSourceTree.Source);
			proj.AddFileToBuild(target, fileGuid);

            proj.AddBuildProperty(target, "OTHER_LDFLAGS", "-all_load -ObjC");
            proj.SetBuildProperty(target, "DEAD_CODE_STRIPPING", "YES");

			File.WriteAllText(projPath, proj.WriteToString());
		}
	}
}
#endif
