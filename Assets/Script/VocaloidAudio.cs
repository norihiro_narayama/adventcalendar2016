﻿using System;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;

#if UNITY_EDITOR_WIN || UNITY_STANDALONE_WIN
using Yamaha.VOCALOID.Windows;
#elif UNITY_EDITOR_OSX || UNITY_STANDALONE_OSX
using Yamaha.VOCALOID.OSX;
#elif UNITY_IOS
using Yamaha.VOCALOID.iOS;
#endif

namespace AdventCalendar2016 {

	[Serializable]
	class NoteDataModel {
	    public int noteLength = 0;
	    public int noteNumber = 0;
	    public string lyricStr = "";
	}
	[Serializable]
	class NoteArrayModel {
	    public List<NoteDataModel> noteArray = new List<NoteDataModel>();
	}

	public class VocaloidAudio:MonoBehaviour {
		public enum RenderState {
			Waiting, Rendering, Rendered, Ready, RenderError, RenderStateLength
		};

		private RenderState rState = RenderState.Waiting;	// レンダリングの状態.
		public RenderState RState {
			get{
				return this.rState;
			}
		}
		
		private AudioSource source = null;
		private Int32 engineHandle = 0;					// 合成エンジンのハンドル.

		private Int16[] renderData = null;				// YVFRenderで合成波形を書き込むための配列.
		private Int32 totalRenderSamples = 0;			// YVFRenderで書き込まれた合成波形の長さ.
		private Int32 audioPosition = 0;

		private YVF.YVFSinger singer;

		private Thread thread = null;

		void FixedUpdate() {
			if ( RState == RenderState.Rendered ){
				if ( totalRenderSamples <= 0 ) {
					// サンプルがひとつ以上ない場合には,AudioClipの生成を行わない.
					return;
				}

				AudioClip clip = AudioClip.Create( "VOCALOID" + gameObject.GetInstanceID(), totalRenderSamples, 1, YVF.YVFSamplingRate, false, OnAudioRead, OnAudioSetPosition );
				source = gameObject.GetComponent<AudioSource>();
				source.clip = clip;

				rState = RenderState.Ready;
			} else if ( RState == RenderState.RenderError ) {
				Delete();
			}
		}

		void OnAudioRead( Single[] data ) {
			for ( Int32 i = 0; i < data.Length; ++i, ++audioPosition ) {
				data[i] = renderData[audioPosition] / 32768.0f; // convert [-32768, 32767] (short) to [-1.0, 1.0) (float).
			}
		}

		void OnAudioSetPosition( Int32 newPosition ) {
			audioPosition = newPosition;
		}
			
		void OnDestroy() {
			if ( thread != null ){
				thread.Abort();
			}
		}

		/**
		 * @brief	再生を停止し，ゲームオブジェクトを破棄する
		 */
		public void Delete() {
			if ( source != null ) {
				source.Stop();
				Destroy( source.clip );
			}
			Destroy( gameObject );
		}
			
		/**
		 * @brief	歌声合成を行うスレッド
		 */
		void threadWork() {
			while ( true ) {
				Thread.Sleep(10);
				if( RState == RenderState.Waiting ) {
					try{
						rState = RenderState.Rendering;
						RenderMelody();
						rState = RenderState.Rendered;
					}
					catch ( VocaloidException e ) {
						YVF.YVFCloseSong(e.Handle);
						rState = RenderState.RenderError;
						print(e);
					}
					finally {
						thread.Abort();
					}
				}
			}
		}

		/**
		 * @brief	エンジンハンドルを設定して，合成を行うスレッドを走らせる
		 * @param	engineHandle	合成を行うエンジンのハンドル
		 * @param	singer			シンガー情報
		 */
		public void SetupRender( Int32 engineHandle, YVF.YVFSinger singer ) {
			this.engineHandle = engineHandle;
			this.singer = singer;

			thread = new Thread(threadWork);
			thread.Start();
		}

		/**
		 * @brief	合成音を再生する	
		 */
		public void Sing() {
			source.Play();
			rState = RenderState.Waiting;
		}

		/**
		 * @brief	シーケンスに基づいて合成を行う
		 */
		public void RenderMelody() {
			Int32 handle = YVF.YVFOpenSong(); 

			Int16 preMeasure;
			if (YVF.YVFGetPreMeasure( handle, out preMeasure ) != YVF.YVFResult.Success){
				throw new VocaloidException( "YVFGetPreMeasure", handle );
			}
			Int32 preTick = VocaloidSystem.MeasureToTick( preMeasure );

			// テンポの編集.
			YVF.YVFTempo tempo = new YVF.YVFTempo();
			tempo.time = 0;
			tempo.bpm = 120;
			if(YVF.YVFEditTempoInMaster( handle, ref tempo ) != YVF.YVFResult.Success){
				throw new VocaloidException( "YVFEditTempoInMaster", handle );
			}

			Int16 track = 1;

			// パートの取得.
			YVF.YVFPartHead partHead = new YVF.YVFPartHead();
			if( YVF.YVFFindPart( handle, track, 0, out partHead ) != YVF.YVFFindResult.Found ) {
				throw new VocaloidException( "YVFFindPart", handle );
			}
			Int32 partHandle = partHead.partHandle;

			// シンガーの編集.
			if ( YVF.YVFSetSingerInPart(handle, partHandle, ref singer ) != YVF.YVFResult.Success ) {
				throw new VocaloidException( "YVFSetSingerInPart", handle );
			}

			// ノートの編集.
			List<VocaloidNote> melody = new List<VocaloidNote>();
			VocaloidNote nt = new VocaloidNote();

			string jsonData = "{\"noteArray\" : [{\"noteLength\" : 240,\"noteNumber\" : 76,\"lyricStr\" : \"じ\"},";
			jsonData += "{\"noteLength\" : 240,\"noteNumber\" : 76,\"lyricStr\" : \"ん\"},";
			jsonData += "{\"noteLength\" : 240,\"noteNumber\" : 76,\"lyricStr\" : \"ぐ\"},";
			jsonData += "{\"noteLength\" : 240,\"noteNumber\" : 76,\"lyricStr\" : \"る\"},";
			jsonData += "{\"noteLength\" : 720,\"noteNumber\" : 76,\"lyricStr\" : \"べ\"},";
			jsonData += "{\"noteLength\" : 240,\"noteNumber\" : 76,\"lyricStr\" : \"る\"},";
			jsonData += "{\"noteLength\" : 240,\"noteNumber\" : 76,\"lyricStr\" : \"じ\"},";
			jsonData += "{\"noteLength\" : 240,\"noteNumber\" : 76,\"lyricStr\" : \"ん\"},";
			jsonData += "{\"noteLength\" : 240,\"noteNumber\" : 76,\"lyricStr\" : \"ぐ\"},";
			jsonData += "{\"noteLength\" : 240,\"noteNumber\" : 76,\"lyricStr\" : \"る\"},";
			jsonData += "{\"noteLength\" : 720,\"noteNumber\" : 76,\"lyricStr\" : \"べ\"},";
			jsonData += "{\"noteLength\" : 240,\"noteNumber\" : 76,\"lyricStr\" : \"る\"},";
			jsonData += "{\"noteLength\" : 480,\"noteNumber\" : 76,\"lyricStr\" : \"す\"},";
			jsonData += "{\"noteLength\" : 480,\"noteNumber\" : 79,\"lyricStr\" : \"ず\"},";
			jsonData += "{\"noteLength\" : 720,\"noteNumber\" : 72,\"lyricStr\" : \"が\"},";
			jsonData += "{\"noteLength\" : 240,\"noteNumber\" : 74,\"lyricStr\" : \"な\"},";
			jsonData += "{\"noteLength\" : 720,\"noteNumber\" : 76,\"lyricStr\" : \"る\"}";
			jsonData += "]}";

			NoteArrayModel noteArrayData = JsonUtility.FromJson<NoteArrayModel>(jsonData);

			int StartTime = preTick;
			for ( int i = 0; i < noteArrayData.noteArray.Count; i++ ) {

				nt.ClearNote();
				nt.Note.noteTime = StartTime;
				nt.Note.noteLength = noteArrayData.noteArray[i].noteLength;
				nt.Note.noteNumber = noteArrayData.noteArray[i].noteNumber;
				nt.Note.lyricStr = noteArrayData.noteArray[i].lyricStr;
				StartTime += nt.Note.noteLength;
				melody.Add( new VocaloidNote(nt) );

			}

			foreach ( VocaloidNote n in melody ) {
				Int32 noteHandle = -1;
				if(YVF.YVFEditNoteInPart( handle, partHandle, ref n.Note, YVF.YVFLang.Japanese, out noteHandle ) != YVF.YVFResult.Success){
					throw new VocaloidException("YVFEditNoteInPart", handle);
				}
			}

			// 開始tickを指定し，MIDI EVENTデータを生成.
			if(YVF.YVFSetupMidiEventsToEONInPart( handle, partHandle, partHead.posTick ) != YVF.YVFResult.Success){
				throw new VocaloidException("YVFSetupMidiEventsToEONInPart", handle);
			}

            Int32 totalFrame = 0;
            if (YVF.YVFGetTotalFrameByPart(partHandle, out totalFrame) != YVF.YVFResult.Success){
                throw new VocaloidException("YVFGetTotalFrameByPart", handle);
            }
			if(totalFrame <= 0){
				throw new VocaloidException("YVFGetTotalFrameByPart", handle);
			}

			// レンダリング処理の開始.
			if( YVF.YVFBeginRender( engineHandle ) != YVF.YVFResult.Success ) {
				throw new VocaloidException( "YVFBeginRender", handle );
			}

			Int32 targetRenderSamples = totalFrame * YVF.YVFSamplesPerFrame;
			renderData = new Int16[targetRenderSamples];
			totalRenderSamples = 0;

			while (true){
				Int32 renderSamples = 0;

				// レンダリング.
				if( YVF.YVFRender( partHandle, renderData, targetRenderSamples, engineHandle, out renderSamples ) != YVF.YVFResult.Success ) {
					throw new VocaloidException( "YVFRender", handle );
				}

				totalRenderSamples += renderSamples;
				if ( renderSamples < targetRenderSamples ){
					break;
				}
			}

			// レンダリング処理の終了.
            if( YVF.YVFEndRender( engineHandle ) != YVF.YVFResult.Success ){
                throw new VocaloidException( "YVFEndRender", handle );
            }

            YVF.YVFCloseSong( handle );
		}
	}
}
